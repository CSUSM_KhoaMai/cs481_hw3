﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NaviPage : ContentPage
    {
        String lastPage = "";
        //This is for tracking the last page the user visited


        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Red;

        }

        public NaviPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NaviPage)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            lastPage = "Main";
        }
        void DogClick(object sender, System.EventArgs e)
        {
            //User designates they are a Dog person
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DogClick)}");
            lastPage = "Dog";
            Navigation.PushAsync(new DogPage());
        }
        void CatClick(object sender, System.EventArgs e)
        {
            //User designates they are a Cat person
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(CatClick)}");
            lastPage = "Cat";
            Navigation.PushAsync(new CatPage());
        }
        void BirdClick(object sender, System.EventArgs e)
        {
            //User indicates they are an Avian fan
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(BirdClick)}");
            lastPage = "Bird";
            Navigation.PushAsync(new BirdPage());
        }
        async void LastClick(object sender, System.EventArgs e)
        {
            //Tracks what page the user last visited
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(LastClick)}");
            bool usersResponse = await DisplayAlert("Low Memory",
                         "Do you remember what button you pressed last?",
                         "Remind Me",
                         "I have a Stable Memory");

            if (usersResponse == true)
            {
                if (lastPage == "Dog")
                    await Navigation.PushAsync(new DogPage());
                else if (lastPage == "Cat")
                    await Navigation.PushAsync(new CatPage());
                else if (lastPage == "Bird")
                    await Navigation.PushAsync(new BirdPage());
            }
        }
    }
}