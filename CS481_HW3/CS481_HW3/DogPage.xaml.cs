﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DogPage : ContentPage
	{
		public DogPage()
		{
			InitializeComponent();

			
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.

		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> dogQuotes = new List<string>(new string[] { "A dog is the only thing on earth that loves you more than you love yourself",
				"When the dog looks at you, the dog is not thinking what kind of a person you are.",
				"No matter how you're feeling, a little dog gonna love you." });
			RandDogText.Text = dogQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Black;

		}
	}
}