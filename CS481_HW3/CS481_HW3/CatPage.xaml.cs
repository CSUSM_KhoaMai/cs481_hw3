﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CatPage : ContentPage
	{
		public CatPage()
		{
			InitializeComponent();
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.

		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> catQuotes = new List<string>(new string[] { "Time spent with cats is never wasted.",
				"Cats choose us, we don't own them.",
				"It is impossible to keep a straight face in the presence of one or more kittens." });
			RandCatText.Text = catQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Yellow;

		}
	}
}