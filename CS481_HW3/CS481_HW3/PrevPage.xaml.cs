﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PrevPage : ContentPage
	{
		public PrevPage(string lastpage)
		{
			//InitializeComponent();
			if (lastpage == "Dog")
				Navigation.PushAsync(new DogPage());
			else if (lastpage == "Cat")
				Navigation.PushAsync(new CatPage());
			else if (lastpage == "Bird")
				Navigation.PushAsync(new BirdPage());
			

		}
	}
}