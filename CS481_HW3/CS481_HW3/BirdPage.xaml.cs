﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BirdPage : ContentPage
	{
		public BirdPage()
		{
			InitializeComponent();
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.
		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> birdQuotes = new List<string>(new string[] { "The reason birds can fly and we can't is simply because they have perfect faith, for to have faith is to have wings.",
				"No bird soars too high if he soars with his own wings.",
				"A bird cannot fly with one wing only. Human space flight cannot develop any further without the active participation of women." });
			RandBirdText.Text = birdQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Green;

		}
	}
}