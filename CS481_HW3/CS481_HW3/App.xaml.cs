﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //point toward navigation page, wrap the nav page so PushAsync will work
            MainPage = new NavigationPage(new NaviPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
